package pe.gob.general.pdainteractor;

/**
 *
 */
public class PdaNaturalPerson extends PdaPerson
{
    /**
     * i.e. Passport,
     *      National Document
     *      Foreigner ID, etc
     */
    String document_type;

    /**
     * String which identifies this person
     */
    String document_id;
}
