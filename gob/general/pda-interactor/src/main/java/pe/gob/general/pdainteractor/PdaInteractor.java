package pe.gob.general.pdainteractor;

import org.springframework.data.annotation.Id;

/**
 * Anything which will interact with our software.
 * It includes people (national, foreigner), companies and also things (IoT)
 */
public class PdaInteractor
{
    @Id
    String id;
}
