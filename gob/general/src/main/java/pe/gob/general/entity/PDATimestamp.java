package pe.gob.general.entity;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class PDATimestamp
{
    /**
     * Absolute time
     * */
    private Instant timestamp;

//    @Indexed
//    private Integer year;
    /**
     * Month of this timestamp
     */
    private Integer month;

    /**
     * Day of this timestamp 1-31
     */
    private Integer day;

    /**
     * Hour of this timestamp 00-23
     */
    private Integer hour;

    /**
     * Minute of this timestamp 00-59
     */
    private Integer minute;
    private Integer second;

    /**
     * DayOfWeek of this timestamp 1:Mon, 2:Tue, 3: Wed, 4:Thu, 5:Fri, 6:Sat, 7:Sun
     */
    private Integer dayOfWeek;

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp)
    {
        this.timestamp = timestamp;
        ZoneId zone = ZoneId.systemDefault();
        ZonedDateTime zonedDateTime = timestamp.atZone(zone);

        setMonth(zonedDateTime.getMonthValue());
        setDay(zonedDateTime.getDayOfMonth());
        setHour(zonedDateTime.getHour());
        setMinute(zonedDateTime.getMinute());
        setSecond(zonedDateTime.getSecond());
        setDayOfWeek(zonedDateTime.getDayOfWeek().getValue());
    }

    public Integer getMonth()
    {
        return month;
    }

    private void setMonth(Integer month)
    {
        this.month = month;
    }

    public Integer getDay()
    {
        return day;
    }

    private void setDay(Integer day)
    {
        this.day = day;
    }

    public Integer getHour()
    {
        return hour;
    }

    private void setHour(Integer hour)
    {
        this.hour = hour;
    }

    public Integer getMinute()
    {
        return minute;
    }

    private void setMinute(Integer minute)
    {
        this.minute = minute;
    }

    public Integer getSecond()
    {
        return second;
    }

    private void setSecond(Integer second)
    {
        this.second = second;
    }

    public Integer getDayOfWeek()
    {
        return dayOfWeek;
    }

    private void setDayOfWeek(Integer dayOfWeek)
    {
        this.dayOfWeek = dayOfWeek;
    }
}
