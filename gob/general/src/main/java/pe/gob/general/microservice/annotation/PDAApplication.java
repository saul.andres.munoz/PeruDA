package pe.gob.general.microservice.annotation;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pe.gob.general.microservice.config.Config;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootApplication
@Configuration
@Import(Config.class)
@EnableEurekaClient       // Para ser registrado y descubierto por el Eureka Server

@EnableCircuitBreaker     // Tolerancia a fallas cuando un microservicio pueda fallar
@EnableTurbine            // Para visualizar el dashboard (Hystrix) de varios microservicios

@EnableDiscoveryClient    // Habilitar este microservicio para ser descubierto en base a su nombre y ya no con el IP:puerto
@EnableSwagger2           // Para generar documentación ce los endpoints
public @interface PDAApplication
{

}
