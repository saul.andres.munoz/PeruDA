package pe.gob.general.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@CompoundIndex(name="MountDay", def = "{'timestamp.month':1, 'timestamp.day':1}")
@CompoundIndex(name="App-Module-Mount-Day", def = "{'application':1, 'module':1, 'timestamp.month':1, 'timestamp.day':1}")

@CompoundIndex(name="timestamp", def = "{'timestamp.timestamp':1}")
@CompoundIndex(name="AppModule", def = "{'application':1, 'module':1}")
@CompoundIndex(name="AppModuleSourceFunc", def = "{'application':1, 'module':1, 'prevFunctionality':1}")

// Para un modulo de un app, corto un tubo vertical (geolocation) y luego corto horizontalmente el tubo (rango de tiempo)
@CompoundIndex(name="AppModule-Geolocation-Time", def = "{'application':1, 'module':1, 'geolocation':'2d', 'timestamp.timestamp':1}")

// Todos las transiciones entre funcionalidades ordenas por tiempo
@CompoundIndex(name="App-Module-SourceFunc-TargetFunc-Time", def = "{'application':1, 'module':1, 'prevFunctionality':1, 'targetFunctionality':1, 'timestamp.timestamp':1}}")

// Todos los clicks de una app, primero corto el tubo de geolocation vertical, luego hago el corte horizontal (rango de tiempo)
@CompoundIndex(name="App-Geolocation-Time", def = "{'application':1, 'geolocation':'2d', 'timestamp.timestamp':1}")

// Todos los clicks de una app, primero corto el tiempo en un rango (plancha con altura), luego hago el corte geolocalizado
@CompoundIndex(name="App-Time-Geolocation", def = "{'application':1, 'timestamp.timestamp':1}, 'geolocation':'2d'")
public class ClickLog
{
    @Id
    private String id;

    @Indexed
    private String application;
    private String module;

    @Indexed
    private PDATimestamp timestamp;

    private String prevFunctionality;

    private String targetFunctionality;

    private String user;

    //@GeoSpatialIndexed
    private Point geolocation;

    private Double bandwidth;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getApplication()
    {
        return application;
    }

    public void setApplication(String application)
    {
        this.application = application;
    }

    public PDATimestamp getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(PDATimestamp timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getPrevFunctionality()
    {
        return prevFunctionality;
    }

    public void setPrevFunctionality(String prevFunctionality)
    {
        this.prevFunctionality = prevFunctionality;
    }

    public String getTargetFunctionality()
    {
        return targetFunctionality;
    }

    public void setTargetFunctionality(String targetFunctionality)
    {
        this.targetFunctionality = targetFunctionality;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getModule()
    {
        return module;
    }

    public void setModule(String module)
    {
        this.module = module;
    }

    public Point getGeolocation()
    {
        return geolocation;
    }

    public void setGeolocation(Point geolocation)
    {
        this.geolocation = geolocation;
    }

    public Double getBandwidth()
    {
        return bandwidth;
    }

    public void setBandwidth(Double bandwidth)
    {
        this.bandwidth = bandwidth;
    }
}
