//package pe.gob.general.microservice;
//
//import org.springframework.boot.web.servlet.error.ErrorController;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.http.HttpServletRequest;
//
//@RestController
//public class BaseController implements ErrorController
//{
//    @RequestMapping("/error")
//    String ErrorHandler(HttpServletRequest request)
//    {
//        String statusCode = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString();
//        if(statusCode.equals("404"))
//            return "--404--";
//        else
//            return "NotHandledYet !";
//    }
//
//    @GetMapping("/ping")
//    String Ping()
//    {
//        return "Pong !";
//    }
//
//    @Override
//    public String getErrorPath()
//    {
//        return null;
//    }
//}
