package pe.gob.general.aspect;

import org.springframework.stereotype.Component;
import pe.gob.general.entity.LogQuery;

import java.util.HashMap;
import java.util.Map;

@Component
public class AnalyticsMap extends HashMap<Long, Map<String, LogQuery>>
{
}
