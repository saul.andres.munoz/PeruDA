package pe.gob.general.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PDARepository<T, ID> extends ReactiveMongoRepository<T, ID>
{
//    List< T, ID> ExeQuery(String query)
//    {
//        timestamp start = getTime();
//        List< T, ID> result = super.ExeQuery();
//        timestamp end = getTime();
//
//        saveQuery(module, "GetInbox", query, start, end)
//    }
}
