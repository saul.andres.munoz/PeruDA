package pe.gob.general.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.gob.general.entity.ClickLog;
import reactor.core.publisher.Flux;

//@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes = MongoConfig.class)
@SpringBootTest
public class TestPDARepository
{
    @Autowired
    ClickLogRepository repository;

    @Test
    public void testListClickLogs()
    {
        Flux<ClickLog> response = repository.findAll();
        Assertions.assertNotNull(response);
    }
}
