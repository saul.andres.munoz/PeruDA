package pe.gob.reniec;

import org.springframework.web.bind.annotation.RequestMapping;
import pe.gob.general.microservice.annotation.PDAController;

@PDAController
public class CitizenMicroservice
{
  @RequestMapping("/prueba3")
  String test() {
    return "prueba";
  }
}
